from datetime import date

from django.db import models
from django.contrib.auth import get_user_model


class Task(models.Model):
    title = models.CharField(max_length=140)
    description = models.TextField()

    start_date = models.DateField(default=date.today(), db_index=True)
    end_date = models.DateField(default=date.today(), db_index=True)

    user = models.ForeignKey(get_user_model())

    completed = models.BooleanField(default=False, editable=False)

    def __unicode__(self):
        return u'%s' % self.title