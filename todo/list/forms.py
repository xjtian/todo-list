from django.forms import ModelForm

from list.models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'description', 'start_date', 'end_date',]