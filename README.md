Django To-Do List
=================

Super simple todo-list app built with Django as an exercise.

Configuration
-------------

First, create a `staging.py` file in the project settings package:

    from todo.settings.base import *

    SECRET_KEY = 'whatever you want'
    STATIC_ROOT = '/home/vagrant/www/public/'
    ALLOWED_HOSTS = ['*']

Then run `vagrant up` in the repository root. After the VM is provisioned, `vagrant ssh` and run

    cd /vagrant/todo
    source ~/venvs/todo/bin/activate
    python manage.py collectstatic --settings=todo.settings.staging
